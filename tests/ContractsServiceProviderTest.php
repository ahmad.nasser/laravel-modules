<?php

namespace AhmadNasser\Modules\Tests;

use AhmadNasser\Modules\Contracts\RepositoryInterface;
use AhmadNasser\Modules\Laravel\LaravelFileRepository;

class ContractsServiceProviderTest extends BaseTestCase
{
    /** @test */
    public function it_binds_repository_interface_with_implementation()
    {
        $this->assertInstanceOf(LaravelFileRepository::class, app(RepositoryInterface::class));
    }
}
