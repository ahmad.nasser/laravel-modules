<?php

namespace AhmadNasser\Modules\Providers;

use Illuminate\Support\ServiceProvider;
use AhmadNasser\Modules\Contracts\RepositoryInterface;
use AhmadNasser\Modules\Laravel\LaravelFileRepository;

class ContractsServiceProvider extends ServiceProvider
{
    /**
     * Register some binding.
     */
    public function register()
    {
        $this->app->bind(RepositoryInterface::class, LaravelFileRepository::class);
    }
}
